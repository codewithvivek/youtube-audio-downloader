# Youtube-Audio-Downloader
A Simple Python script that is able to download YouTube video and convert them into MP3.

Download following dependencies:
1) pip install pytube
2) pip install moviepy

To run the program:     
python3 main.py
