import pytube
import youtube_downloader
import file_converter
import os 

print("Welcome to YouTube Audio Downloader")
print("Loading...")

link = youtube_downloader.input_links()

print("Downloading the video...")
filename = youtube_downloader.download_video(link, 'low')
print("Converting to MP3...")
file_converter.convert_to_mp3(filename)
os.remove(filename)